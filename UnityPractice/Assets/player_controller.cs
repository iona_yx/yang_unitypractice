﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player_controller : MonoBehaviour {

    Rigidbody rb;
    public float Speed;
    int count = 0;
    public Text countText;
    public Text WinText;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f,moveVertical);

        rb.AddForce(movement*Speed);
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Pickup"))
        {
            count += 1;
            SetCountText();
            
            Destroy(other.gameObject);
      
        }
    }
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count>=12)
        {
            WinText.text = "You Win";
        }

    }

}
